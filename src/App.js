import './App.css';
import { UserProvider } from './UserContext';
import AppNavbar from './AppNavbar';
import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React from 'react';

import Login from "./pages/Login";
import Register from "./pages/Register";
import Home from "./pages/Home";
import Logout from "./pages/Logout";
import Product from './pages/Product';
import AdminDashboard from './pages/AdminDashboard';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null,
    name: null
  });
  
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log("user: " + user.name);
    console.log("localStorage" + localStorage);
  }, [user])

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container fluid style={{ 'padding': '0', 'height': '100% !important', 'font-family': 'Sans-Serif'}}>
            <Routes>
              <Route path='/' element={<Home />} />
              <Route path='/products' element={<Product />} />
              <Route path='/login' element={<Login />} />
              <Route path='/register' element={<Register />} />
              <Route path='/logout' element={<Logout />} />
              <Route path='/admin-dashboard' element={<AdminDashboard />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
