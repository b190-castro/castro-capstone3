import { useContext, Fragment } from 'react';
import Nav from 'react-bootstrap/Nav';
//import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';
import AppLogo from './assets/app-logo.jpg'; // with import
import { Link } from 'react-router-dom';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	const navBarStyle = {
		'background-color': '#BB4D00',
		'height': '60px',
		'padding-left': '20px'
	}

	const navLinkStyle = {
		'color': 'white',
		'font-size': '20px',
		'font-weight': 'bold',
		'padding-left': '20px',
		'padding-right': '20px'
	}

	console.log("userId: " + user.id)

	return (
		<nav class="navbar navbar-expand-lg navbar-light" style={navBarStyle}>
			<div class="container-fluid">
				<Nav.Link class="navbar-brand" as={ Link } to='/' >
					<img src={AppLogo} alt='hello' style={{ 'width': '150px', 'height': '45px' }} />
				</Nav.Link>
				<div class="collapse navbar-collapse" id="navbarNavAltMarkup" style={{ 'position': 'relative' }}>
					<div class="navbar-nav" style={{ 'position': 'absolute' }}>
						<Nav.Link class="nav-link" style={navLinkStyle} as={ Link } to='/'>Home</Nav.Link>
						<Nav.Link class="nav-link" style={navLinkStyle} as={ Link } to='/products'>Products</Nav.Link>

						{(user.isAdmin) ?
							<Fragment>
								<Nav.Link class="nav-link" style={navLinkStyle} as={ Link } to='/admin-dashboard'>Admin Dashboard</Nav.Link>
							</Fragment> :
							<Fragment>
								
							</Fragment>
						}
					</div>
					<div class="navbar-nav" style={{ 'position': 'absolute', 'left': 'auto', 'right': '0px' }}>
						{(user.id !== null) ?
							<Fragment>
								<div class="nav-link" style={navLinkStyle}>Hello, {user.name}</div>
								<a class="nav-link" style={navLinkStyle} href="/logout">Logout</a>
							</Fragment>
							:
							<Fragment>
								<Nav.Link class="nav-link" style={navLinkStyle} as={ Link } to='/login'>Login</Nav.Link>
								<Nav.Link class="nav-link" style={navLinkStyle} as={ Link } to='/register'>Register</Nav.Link>
							</Fragment>
						}
					</div>
				</div>
			</div>
		</nav>
	)
}