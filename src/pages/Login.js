import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Footer from './Footer';

export default function Login() {
	const { user, setUser } = useContext(UserContext);

	const containerFluidStyle = {
		'padding': '0 !important',
		'height': '100% !important',
		'background-repeat': 'no-repeat',
		'background-size': '100% 100%',
		'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")'
	}

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	const retrieveUserDetails = (token) => {
		fetch('https://blooming-hamlet-09049.herokuapp.com/users/user-id', {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				console.log("retrieveUserDetails: " + data.email);
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
					email: data.email,
					name: data.firstName + " " + data.lastName
				})

				console.log("name: " + data.firstName + " " + data.lastName)
			})
	}

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	function loginUser(e) {
		e.preventDefault();

		fetch('https://blooming-hamlet-09049.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data.access !== null) {
					localStorage.setItem('token', data.access)
					retrieveUserDetails(data.access);

					Swal.fire({
						title: "Login successful",
						icon: "success",
						text: "Welcome to PlayShop!"
					})

					console.log("user.email: " + user.email)
				} else {
					Swal.fire({
						title: "Authentication failed",
						icon: "error",
						text: "Check your login credentials and try again."
					})
				}

			});

		setEmail('');
		setPassword('');
	}


	return (
		(user.id !== null) ?
			<Navigate to='/' />
			:
			<div class="container-fluid" style={containerFluidStyle}>
				<div class="row" style={{ 'min-height': '800px' }} >
					<div class="col-4">
					</div>
					<div class="col-4" >
						<div class="login-box" style={{ 'height': '330px', 'width': '100%', 'background-color': '#E6E9EB', 'border-radius': '20px', 'margin-top': '80px', 'padding':'20px' }}>
							<Form onSubmit={(e) => loginUser(e)}>
								<Form.Group controlId="userEmail">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Email address</Form.Label>
									<Form.Control
										type="email"
										placeholder="Enter email"
										value={email}
										onChange={e => setEmail(e.target.value)}
										required
									/>
									<Form.Text className="text-muted">
										We'll never share your email with anyone else.
									</Form.Text>
								</Form.Group>

								<Form.Group controlId="password1">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Password"
										value={password}
										onChange={e => setPassword(e.target.value)}
										required
									/>
								</Form.Group>

								{isActive ?
									<Button variant="primary" type="submit" id="submitBtn" style={{ 'margin-top':'15px' }}>Login</Button>
									:
									<Button variant="primary" type="submit" id="submitBtn" disabled style={{ 'margin-top':'15px' }}>Login</Button>
								}
							</Form>
						</div>
					</div>
					<div class="col-4">
					</div>
				</div>

				<div class="row" style={{ 'padding': '0 !important' }}>
					<Footer />
				</div>
			</div>
	);
}