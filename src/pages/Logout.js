import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			email: null,
			isAdmin: null,
			name: null
		});

		Swal.fire({
			title: "Thank you for shopping!",
			icon: "success",
		})
	})

	return (
		<Navigate to='/' />
	)
}