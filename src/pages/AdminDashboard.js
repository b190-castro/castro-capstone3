import Footer from './Footer';
import React, { useEffect, useState, Fragment } from 'react';
import { Button, Dropdown, Form } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import AdminProductCard from '../components/AdminProductCard';
import Swal from 'sweetalert2';
import OrderCard from '../components/OrderCard';

export default function AdminDashboard() {
    const accessToken = localStorage.getItem('token')

    const [showUserOrder, setShowUserOrder] = useState(false);

    const containerFluidStyle = {
        'padding': '0 !important',
        'height': '100% !important',
        'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")',
        'background-repeat': 'no-repeat',
        'background-size': '100% 100%'
    }

    const adminDashboardStyle = {
        'font-size': '70px',
        'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")',
        'justify-content': 'center',
        'color': 'white'
    }

    const adminRowStyle = {
        'min-height': '800px',
        'padding': '30px'
    }

    const adminProductCardsStyle = {
        'display': 'flex',
        //'flex-direction': 'row',
        'flex-wrap': 'wrap',
        'flex-direction': 'column',
        'justify-content': 'space-around',
    }

    const [products, setProducts] = useState([]);
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        // since we used .env.local, we have to restart the application because environment variables take effect at the initial start of the application
        fetch('https://blooming-hamlet-09049.herokuapp.com/products/all-active', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log("All Products: " + data)
                setProducts(data.map(product => {
                    return (<AdminProductCard key={product._id} productProp={product} />)
                }
                ))
            })
    }, [])

    useEffect(() => {

        fetch('https://blooming-hamlet-09049.herokuapp.com/orders/all-orders', {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log("All Orders: " + data)
                setOrders(data.map(order => {
                    return (<OrderCard key={order._id} orderProp={order} />)
                }))
            })
    }, [], [])

    const numberFormat = (value) =>
        new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'PHP'
        }).format(value);

    function addProduct(e, name, description, price, quantityItem, imageUrl) {
        e.preventDefault()

        fetch(`https://blooming-hamlet-09049.herokuapp.com/products/add-product`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                remainingQuantity: quantityItem,
                imageUrl: imageUrl
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.code == "200") {
                    setModalShow(false)
                    Swal.fire({
                        title: "Congratulations!",
                        icon: "Success",
                        text: "You have successfully added a product!"
                    })
                } else {
                    console.log(data.response)
                }
            })
    }

    const [modalShow, setModalShow] = useState(false);

    function AddProductModal(modalProp) {

        const [productName, setProductName] = useState('');
        const [productDescription, setProductDescription] = useState('');
        const [productPrice, setProductPrice] = useState('');
        const [productImage, setProductImage] = useState('');
        const [n, setN] = useState(1);

        const [isActive, setIsActive] = useState(false);

        useEffect(() => {
            if (productName !== '' && productDescription !== '' && productPrice !== '' && productImage !== '') {
                setIsActive(true);
            } else {
                setIsActive(false);
            }
        }, [productName, productDescription, productPrice, productImage]);

        var quantity = []

        for (let i = 1; i <= 100; i++) {
            quantity.push(i)
        }

        return (
            <Modal
                {...modalProp}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        <h3>ADD PRODUCT</h3>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                type="productName"
                                placeholder="Enter Product Name"
                                value={productName}
                                onChange={e => setProductName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                                type="productDescription"
                                placeholder="Enter Product Description"
                                value={productDescription}
                                onChange={e => setProductDescription(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <div>
                            Quantity
                            <Dropdown>
                                <Dropdown.Toggle variant="light" id="dropdown-basic">
                                    {n}
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {quantity.map(num => {
                                        return (<Dropdown.Item onClick={() => setN(num)}>{num}</Dropdown.Item>)
                                    })}
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Product Price</Form.Label>
                            <Form.Control
                                type="productPrice"
                                placeholder="Enter Product Price"
                                value={productPrice}
                                onChange={e => setProductPrice(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="productImage">
                            <Form.Label>Product Image URL</Form.Label>
                            <Form.Control
                                type="productImage"
                                placeholder="Enter Product Image URL"
                                value={productImage}
                                onChange={e => setProductImage(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='success' onClick={(e) => addProduct(e, productName, productDescription, productPrice, n)}>Confirm</Button>
                    <Button variant='dark' onClick={modalProp.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function toggleShowUserOrder(e) {

        if (showUserOrder) {
            setShowUserOrder(false);
        } else {
            setShowUserOrder(true);
        }
    }

    return (
        <div class="container-fluid" style={{ containerFluidStyle }}>
            <div class="row" style={adminDashboardStyle} >
                Admin Dashboard
                <div style={{ 'display': 'flex', 'justify-content': 'center', 'padding': '20px' }}>
                    <button class="btn btn-primary" style={{ 'margin': '20px' }} onClick={() => setModalShow(true)}>Add Product</button>
                    {showUserOrder ?
                        <button class="btn btn-success" style={{ 'margin': '20px' }} onClick={(e) => toggleShowUserOrder(e)}>Show All Products</button>
                        :
                        <button class="btn btn-success" style={{ 'margin': '20px' }} onClick={(e) => toggleShowUserOrder(e)}>Show User Orders</button>
                    }
                </div>
            </div>
            <div class="row" style={adminRowStyle}>
                {showUserOrder ?
                    <div class="product-cards" style={adminProductCardsStyle}>
                        <Fragment>
                            <div class="row-label" style={{ 'font-size': '30px', 'font-weight': 'bolder', 'color': 'black', 'text-align':'center' }}>
                                All Orders
                            </div>
                            {orders}
                        </Fragment>
                    </div>
                    :
                    <div class="product-cards" style={adminProductCardsStyle}>
                        <Fragment>
                            <div class="row-label" style={{ 'font-size': '30px', 'font-weight': 'bolder', 'color': 'black', 'text-align':'center' }}>
                                All Products
                            </div>
                            {products}
                        </Fragment>
                    </div>
                }
            </div>
            <div class="row" style={{ 'padding': '0 !important' }}>
                <Footer />
            </div>
            <AddProductModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
}