import React, { Component } from 'react';

export default function Footer() {

    return (
        <div bgColor='light' className='text-center text-lg-start text-muted' style={{'padding':'0 !important', 'background-color':'#4c5760'}}>
            <div className='text-center p-4' style={{ 'color': 'white' }}>
                © 2022 Copyright: <b>Coel John Castro - for educational purposes only </b>
            </div>
        </div>
    )

}