import React, { Component, Fragment, useEffect, useState } from 'react';
import Footer from './Footer';
import ProductCard from '../components/ProductCard';

export default function Product() {

    const containerFluidStyle = {
        'padding': '0 !important',
        'height': '100% !important',
        'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")',
        'background-repeat': 'no-repeat',
        'background-size': '100% 100%'
    }

    const allProductsLabelStyle = {
        'height': '200px',
        'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")',
        'color': 'white',
        'padding': '30px',
        'font-size': '80px'
    }

    const productsRowStyle = {
        'min-height': '800px',
        'padding': '30px'
    }

    const productCardsStyle = {
        'display': 'flex',
        'flex-direction': 'row',
        'flex-wrap': 'wrap',
        'justify-content': 'space-around',
    }

    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://blooming-hamlet-09049.herokuapp.com/products/all-active', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                setProducts(data.map(product => {
                    return (<ProductCard key={product._id} productProp={product} />)
                }
                ))
            })
    }, [])

    return (
        <div class="container-fluid" style={{ containerFluidStyle }}>
            <div class="row" style={allProductsLabelStyle} >
                All Products
            </div>

            <div class="row" style={productsRowStyle} >
                <div class="product-cards" style={productCardsStyle}>
                    <Fragment>
                        {products}
                    </Fragment>
                </div>
            </div>

            <div class="row" style={{ 'padding': '0 !important' }}>
                <Footer />
            </div>
        </div>
    )

}