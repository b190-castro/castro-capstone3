import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import Footer from './Footer';

export default function Register() {

	const containerFluidStyle = {
		'padding': '0 !important',
		'height': '100% !important',
		'background-repeat': 'no-repeat',
		'background-size': '100% 100%',
		'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")'
	}

	const navigate = useNavigate();
	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [isActive, setIsActive] = useState(false);

	// to check if we have successfully implemented the 2-way binding
	/*
		whenever a state of a component changes, the component renders the whole component executing the code found in the component itself.
		e.target.value allows us to gain access the input field's current value to be used when submitting form data
	*/

	console.log(email);
	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	useEffect(() => {
		if (!isNaN(mobileNumber) && mobileNumber.length === 11) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	function registerUser(e) {
		e.preventDefault();
		fetch('https://blooming-hamlet-09049.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
			.then(res => res.json())
			.then(data => {
				if (!data.isSuccessful) {
					Swal.fire({
						title: "Duplicate mail found",
						icon: "error",
						text: "Please provide a different email"
					})
				} else {
					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "You may now login your credentials!"
					})
					setEmail('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');
					setPassword1('');
					setPassword2('');

					navigate('/login');
				}
			})
	};

	return (
		(user.email !== null) ?
			<Navigate to='/products' />
			:
			// <Form onSubmit={(e) => registerUser(e)}>
			// 	<Form.Group controlId="firstName">
			// 		<Form.Label>First name</Form.Label>
			// 		<Form.Control
			// 			type="firstName"
			// 			placeholder="Enter first name"
			// 			value={firstName}
			// 			onChange={e => setFirstName(e.target.value)}
			// 			required
			// 		/>
			// 	</Form.Group>

			// 	<Form.Group controlId="lastName">
			// 		<Form.Label>Last name</Form.Label>
			// 		<Form.Control
			// 			type="lastName"
			// 			placeholder="Enter last name"
			// 			value={lastName}
			// 			onChange={e => setLastName(e.target.value)}
			// 			required
			// 		/>
			// 	</Form.Group>

			// 	<Form.Group controlId="userEmail">
			// 		<Form.Label>Email address</Form.Label>
			// 		<Form.Control
			// 			type="email"
			// 			placeholder="Enter email"
			// 			value={email}
			// 			onChange={e => setEmail(e.target.value)}
			// 			required
			// 		/>
			// 		<Form.Text className="text-muted">
			// 			We'll never share your email with anyone else.
			// 		</Form.Text>
			// 	</Form.Group>

			// 	<Form.Group controlId="mobileNumber">
			// 		<Form.Label>Mobile number</Form.Label>
			// 		<Form.Control
			// 			type="mobileNumber"
			// 			placeholder="Enter mobile number"
			// 			value={mobileNumber}
			// 			onChange={e => setMobileNumber(e.target.value)}
			// 			required
			// 		/>
			// 	</Form.Group>

			// 	<Form.Group controlId="password1">
			// 		<Form.Label>Password</Form.Label>
			// 		<Form.Control
			// 			type="password"
			// 			placeholder="Password"
			// 			value={password1}
			// 			onChange={e => setPassword1(e.target.value)}
			// 			required
			// 		/>
			// 	</Form.Group>

			// 	<Form.Group controlId="password2">
			// 		<Form.Label>Verify Password</Form.Label>
			// 		<Form.Control
			// 			type="password"
			// 			placeholder="Password"
			// 			value={password2}
			// 			onChange={e => setPassword2(e.target.value)}
			// 			required
			// 		/>
			// 	</Form.Group>
			// 	{isActive ?
			// 		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			// 		:
			// 		<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			// 	}
			// </Form>
			<div class="container-fluid" style={containerFluidStyle}>
				<div class="row" style={{ 'min-height': '800px' }} >
					<div class="col-4">
					</div>
					<div class="col-4" >
						<div class="login-box" style={{ 'height': '700px', 'width': '100%', 'background-color': '#E6E9EB', 'border-radius': '20px', 'margin-top': '60px', 'padding': '20px' }}>
							<Form onSubmit={(e) => registerUser(e)}>
								<Form.Group controlId="firstName">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>First name</Form.Label>
									<Form.Control
										type="firstName"
										placeholder="Enter first name"
										value={firstName}
										onChange={e => setFirstName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="lastName">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Last name</Form.Label>
									<Form.Control
										type="lastName"
										placeholder="Enter last name"
										value={lastName}
										onChange={e => setLastName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="userEmail">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Email address</Form.Label>
									<Form.Control
										type="email"
										placeholder="Enter email"
										value={email}
										onChange={e => setEmail(e.target.value)}
										required
									/>
									<Form.Text className="text-muted">
										We'll never share your email with anyone else.
									</Form.Text>
								</Form.Group>

								<Form.Group controlId="mobileNumber">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Mobile number</Form.Label>
									<Form.Control
										type="mobileNumber"
										placeholder="Enter mobile number"
										value={mobileNumber}
										onChange={e => setMobileNumber(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password1">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Password"
										value={password1}
										onChange={e => setPassword1(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password2">
									<Form.Label style={{ 'font-size':'20px', 'font-weight':'bolder', 'margin-top':'15px' }}>Verify Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Password"
										value={password2}
										onChange={e => setPassword2(e.target.value)}
										required
									/>
								</Form.Group>
								{isActive ?
									<Button variant="primary" type="submit" id="submitBtn" style={{ 'margin-top':'15px' }}>Submit</Button>
									:
									<Button variant="primary" type="submit" id="submitBtn" disabled style={{ 'margin-top':'15px' }}>Submit</Button>
								}
							</Form>
						</div>
					</div>
					<div class="col-4">
					</div>
				</div>

				<div class="row" style={{ 'padding': '0 !important' }}>
					<Footer />
				</div>
			</div>
	);
}