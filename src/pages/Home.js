import React from 'react';
import { Carousel } from "react-bootstrap";
import { bestSellerImages, newArrivals } from '../data/ImageData';
import Footer from './Footer';

export default function Home() {

    var bestSellerImageThumbnails = []
    bestSellerImageThumbnails = bestSellerImages

    const containerFluidStyle = {
        'padding': '0 !important',
        'height': '100% !important',
        'background-image': 'url("https://www.wallpaperflare.com/static/131/568/26/discord-game-console-controller-wallpaper.jpg")', 
        'background-repeat': 'no-repeat', 
        'background-size': '100% 100%'
    }

    const carouselStyle = {
        'width': '100%',
        'height': '100px',
        'margin': '0 auto',
        'margin-top': '10px'
    }

    const bestSellerStyle = {
        'height': '400px',
        'padding': '40px'
    }

    const bestSellerCatalogStyle = {
        'display': 'flex',
        'flex-direction': 'row',
        'flex-wrap': 'wrap',
        'justify-content': 'space-between'
    }

    function bestSellerImage(backgroundImage) {
        return (
            {
                'height': '230px',
                'width': '230px',
                'background-image': 'url(' + backgroundImage + ')',
                'background-size': '100% 100%',
                'background-repeat': 'no-repeat',
                'background-position': 'center'
            }
        )
    }

    function newArrivalsImage(backgroundImage) {
        return (
            {
                'height': '230px',
                'width': '230px',
                'background-image': 'url(' + backgroundImage + ')',
                'background-size': '100% 100%',
                'background-repeat': 'no-repeat',
                'background-position': 'center'
            }
        )
    }

    return (
        <div class="container-fluid" style={containerFluidStyle}>
            <div class="row" style={{ 'height': '550px' }} >
                <div class="carousel" style={carouselStyle}>
                    <Carousel>
                        <Carousel.Item>
                            <img
                                src='https://cdn.shopify.com/s/files/1/0442/2749/4055/files/DUALSENSE-CONTROLLER.jpg?v=1642115649'
                                alt='1'
                                class="d-block w-100"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                src='https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Curved-UltraWide_--Monitor---eCom-Sliding_1800x.jpg?v=1662607466'
                                alt='2'
                                class="d-block w-100"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                src='https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-Ayaneo-Air_1800x.jpg?v=1662098492'
                                alt='3'
                                class="d-block w-100"
                            />
                        </Carousel.Item>
                    </Carousel>
                </div>
            </div>
            <div class="row" style={bestSellerStyle}>
                <div class="row-label" style={{ 'font-size': '30px', 'font-weight': 'bolder', 'color': 'white' }}>
                    Best Sellers
                </div>
                <div class="best-seller-catalog" style={bestSellerCatalogStyle}>
                    {
                        bestSellerImageThumbnails.map(image => (
                            <div class="best-seller-thumbnails" style={bestSellerImage(image)} />
                        ))
                    }
                </div>
            </div>
            <div class="row" style={bestSellerStyle}>
                <div class="row-label" style={{ 'font-size': '30px', 'font-weight': 'bolder', 'color': 'white' }}>
                    New Arrivals
                </div>
                <div class="best-seller-catalog" style={bestSellerCatalogStyle}>
                    {
                        newArrivals.map(image => (
                            <div class="best-seller-thumbnails" style={newArrivalsImage(image)} />
                        ))
                    }
                </div>
            </div>
            <div class="row" style={{'padding':'0 !important'}}>
                <Footer />
            </div>
        </div>
    )
}