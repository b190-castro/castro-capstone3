import { Card, Button, Dropdown, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';
import { useNavigate } from 'react-router-dom';

export default function AdminProductCard({ productProp }) {

    const accessToken = localStorage.getItem('token');

    const { name, description, price, _id, remainingQuantity, imageUrl } = productProp;

    const numberFormat = (value) =>
        new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'PHP'
        }).format(value);

    function updateProduct(e, productId, name, description, price, quantityItem) {
        e.preventDefault()

        console.log("token: " + accessToken)
        console.log("Clicked: " + productId)
        console.log("name selected: " + name)
        console.log("description selected: " + description)
        console.log("price selected: " + price)
        console.log("quantity selected: " + quantityItem)

        fetch(`https://blooming-hamlet-09049.herokuapp.com/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                remainingQuantity: quantityItem
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.code == "200") {
                    setModalShow(false)
                    Swal.fire({
                        title: "Congratulations!",
                        icon: "Success",
                        text: "You have successfully updated the product!"
                    })
                } else {
                    console.log(data.response)
                }
            })
    }

    function ProductOrderModal(modalProp) {

        const [productName, setProductName] = useState(name);
        const [productDescription, setProductDescription] = useState(description);
        const [productPrice, setProductPrice] = useState(price);
        const [n, setN] = useState(remainingQuantity);

        const [isActive, setIsActive] = useState(false);

        useEffect(() => {
            if (productName !== '' && productDescription !== '' && productPrice !== '') {
                setIsActive(true);
            } else {
                setIsActive(false);
            }
        }, [productName, productDescription, productPrice]);

        var quantity = []

        for (let i = 1; i <= remainingQuantity; i++) {
            quantity.push(i)
        }

        return (
            <Modal
                {...modalProp}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        <h3>UPDATE PRODUCT INFORMATION</h3>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                type="productName"
                                placeholder="Enter Product Name"
                                value={productName}
                                onChange={e => setProductName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                                type="productDescription"
                                placeholder="Enter Product Description"
                                value={productDescription}
                                onChange={e => setProductDescription(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <div>
                            Quantity
                            <Dropdown>
                                <Dropdown.Toggle variant="light" id="dropdown-basic">
                                    {n}
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {quantity.map(num => {
                                        return (<Dropdown.Item onClick={() => setN(num)}>{num}</Dropdown.Item>)
                                    })}
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Product Price</Form.Label>
                            <Form.Control
                                type="productPrice"
                                placeholder="Enter Product Price"
                                value={productPrice}
                                onChange={e => setProductPrice(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='success' onClick={(e) => updateProduct(e, _id, productName, productDescription, productPrice, n)}>Confirm</Button>
                    <Button variant='dark' onClick={modalProp.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function disableProduct(e, productId) {
        e.preventDefault()

        fetch(`https://blooming-hamlet-09049.herokuapp.com/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.code == "200") {
                    setModalShow(false)
                    Swal.fire({
                        title: "Disable Product",
                        icon: "Success",
                        text: "You have successfully archived the product!"
                    })
                } else {
                    console.log(data.response)
                }
            })
    }

    const [modalShow, setModalShow] = useState(false);

    return (
        <Card class="container-fluid" style={{ 'margin': '20px', 'margin-left': '150px', 'margin-right': '150px' }}>
            <div class="row">
                <div class="col-6">
                    <Card.Body>
                        <Card.Title><h4 style={{ 'color': '#BB4D00', 'font-weight': 'bolder' }}>{name}</h4></Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                    </Card.Body>
                </div>
                <div class="col-6" style={{ 'display': 'flex', 'margin': 'auto' }}>
                    <div class="buttons mx-auto" style={{ 'float': 'right', 'display': 'flex' }}>
                        <Button variant='primary' style={{ 'margin': '10px', 'padding-left': '3.75rem', 'padding-right': '3.75rem' }} onClick={() => setModalShow(true)}>Update</Button>
                        <Button variant='danger' style={{ 'margin': '10px', 'padding-left': '3.75rem', 'padding-right': '3.75rem' }} onClick={(e) => disableProduct(e, _id)}>Disable</Button>
                    </div>
                </div>
            </div>
            <ProductOrderModal
                key={productProp._id}
                modalProp={productProp}
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </Card>
    )
}