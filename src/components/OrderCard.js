import { Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function OrderCard({ orderProp }) {

    const accessToken = localStorage.getItem('token')

    const { totalAmount, purchasedOn, buyerId, products, quantity } = orderProp;

    const numberFormat = (value) =>
    new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'PHP'
    }).format(value);

    const [userEmail, setUserEmail] = useState('');

    useEffect(() => {
        fetch(`https://blooming-hamlet-09049.herokuapp.com/users/user-details/${buyerId}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                setUserEmail(data.email)
            })

    }, [userEmail])

    return (
        <Card class="container-fluid" style={{ 'margin': '20px' }}>
            <div class="row">
                <div class="order-by-user" style={{'background-color': '#373A3C', 'height': '50px', 'padding':'10px', 'color':'white'}}>
                    <h4>Orders for user <b style={{'color':'#BB4D00'}}>{userEmail}</b></h4>
                </div>
                <div class="order-by-user" style={{'margin-top':'15px'}} >
                    <h5>Purchased on {purchasedOn.substring(0,10)}</h5>
                </div>
                {
                    products.map(product => {
                        return (
                            <Card.Body style={{'margin-left':'15px'}}>
                                <Card.Title>{product.name} - Quantity: {quantity}</Card.Title>
                                <Card.Subtitle>Total: <b style={{'color':'#BB4D00'}}>{numberFormat(totalAmount)}</b></Card.Subtitle>
                            </Card.Body>
                        )
                    })
                }
            </div>
        </Card>
    )

}