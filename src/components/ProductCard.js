import { Button, Dropdown } from 'react-bootstrap';
import { useState, Fragment } from 'react';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';

// we can try to destructure the props object recieved from the parent component(Courses.js) so that the main object from the mock database will be accessed
export default function ProductCard({ productProp }) {

    const accessToken = localStorage.getItem('token')

    console.log("accessToken: " + accessToken)

    const { name, description, price, _id, remainingQuantity, imageUrl } = productProp;

    const numberFormat = (value) =>
        new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'PHP'
        }).format(value);

    function checkoutOrder(e, productId, quantityItem) {
        e.preventDefault()

        console.log("token: " + accessToken)
        console.log("Clicked: " + productId)
        console.log("quantity selected: " + quantityItem)

        fetch('https://blooming-hamlet-09049.herokuapp.com/orders/add-to-bag', {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
                product: productId,
                quantity: quantityItem
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log("addtoBag")
                if (data.code === "200") {
                    setModalShow(false)
                    Swal.fire({
                        title: "Congratulations!",
                        icon: "Success",
                        text: "Order Created!"
                    })
                } else {
                    console.log(data.response)
                }
            })
    }

    function ProductOrderModal(modalProp) {
        const [n, setN] = useState(1);

        var quantity = []

        for (let i = 1; i <= remainingQuantity; i++) {
            quantity.push(i)
        }

        console.log("modalProp: " + modalProp)
        console.log("modalProp ID: " + _id)

        return (
            <Modal
                {...modalProp}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {name}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>{numberFormat(price)}</h4>
                    <p>
                        {description}
                    </p>
                    <div>
                        <Dropdown>
                            <Dropdown.Toggle variant="light" id="dropdown-basic">
                                Quantity: {n}
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                {quantity.map(num => {
                                    return (<Dropdown.Item onClick={() => setN(num)}>{num}</Dropdown.Item>)
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </Modal.Body>


                <Modal.Footer>
                    {(accessToken != null) ?
                        <Fragment>
                            <Button variant='success' onClick={(e) => checkoutOrder(e, _id, n)}>Checkout</Button>
                        </Fragment> :
                        <Fragment>

                        </Fragment>
                    }
                    <Button variant='dark' onClick={modalProp.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    const [modalShow, setModalShow] = useState(false);

    return (
        <div class="card" style={{ 'width': '18rem', 'margin': '20px', 'background-color': '#F8F8F8' }}>
            <img src={imageUrl} class="card-img-top" alt="..." style={{ 'height': '200px', 'width': '200px', 'margin-left': 'auto', 'margin-right': 'auto' }} />
            <div class="card-body" style={{ 'position': 'relative' }}>
                <h5 class="card-title" style={{ 'font-weight': 'bold' }}>{name}</h5>
                <h5 class="card-title">{numberFormat(price)}</h5>
                <p class="card-text" style={{ 'min-height': '100px' }}>{description}</p>
                <div class="user-options" style={{ 'display': 'flex', 'justify-content': 'space-evenly' }}>
                    <button href="#" class="btn btn-primary" style={{ 'width': '100%' }} onClick={() => setModalShow(true)}>Details</button>
                </div>
            </div>
            <ProductOrderModal
                key={productProp._id}
                modalProp={productProp}
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    )
}