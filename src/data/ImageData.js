export const bestSellerImages = [
    "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/PS2-Versions.jpg/1920px-PS2-Versions.jpg",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/products/3_a16a50c1-57e2-41b8-a7da-2d337637f2e2_900x.jpg?v=1659521012",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/products/0-02-06-d5f112670cb05370a7d8aabb1913d69170f3be47a426c6a50dea9251ea595818_2615e20af632b58d_9ee5555f-777a-4f9e-a693-81462188a347_900x.jpg?v=1661003329",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/products/619BkvKW35L._SL1500_900x.jpg?v=1627450491",
    "https://pinoygamer.ph/attachments/1654486554820-png.10760/",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/products/GPD-XP_900x.jpg?v=1660546556"
]

export const newArrivals = [
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-dl2_212x.jpg?v=1652695700",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-arceus_06d3c761-437f-435c-afc1-472a5d027767_212x.jpg?v=1652695715",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-uncharted_212x.jpg?v=1652695727",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-rainbowx_212x.jpg?v=1652695740",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-gta_212x.jpg?v=1652695754",
    "https://cdn.shopify.com/s/files/1/0355/8296/7943/files/Datablitz-pokemon_212x.jpg?v=1652695766"
]